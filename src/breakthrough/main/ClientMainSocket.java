package breakthrough.main;

import breakthrough.domain.Breakthrough;
import breakthrough.domain.BreakthroughBrokerProxy;
import breakthrough.ui.ClientInterpreter;
import frs.broker.ipc.socket.SocketClientRequestHandler;
import frs.broker.marshall.json.StandardJSONRequestor;

/**
 * Client for breakthrough, using socket communication.
 * Hardcoded for port 37321, host given as argument.
 */
public class ClientMainSocket {
    public ClientMainSocket(String host, int port) {
        // Create the game
        Breakthrough game = new BreakthroughBrokerProxy(new StandardJSONRequestor(new SocketClientRequestHandler(host, port)));

        // Welcome
        System.out.println("=== Client Socket. Host = "
                + host + " ===");
        // And start the interpreter...
        ClientInterpreter interpreter =
                new ClientInterpreter(game,
                        System.in, System.out);
        interpreter.readEvalLoop();
    }

    public static void main(String args[]) {
        new ClientMainSocket(args[0], 37321);
    }
}
