package breakthrough.main;

import breakthrough.domain.Breakthrough;
import breakthrough.domain.RESTProxy;
import breakthrough.ui.ClientInterpreter;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Client for breakthrough, using REST.
 */
public class ClientMainREST {
    public static void main(String args[]) throws UnirestException {
        System.out.println("=== Client REST ===");
        String host = args[0];
        String op = args[1];
        String location = args[2];

        String fullUrl = "http://" + host + ":4567";

        System.out.println(" Op: " + op + ", Host: " + host + ", GameId: " + location);

        // TODO: Replace the concrete type of the game with your
        // REST proxy type
        Breakthrough game;

        if (op.equals("create")) {
            game = new RESTProxy(fullUrl);
            // TODO: Create a Breakthrough REST proxy that CREATES a game resource
        } else {
            game = new RESTProxy(Integer.parseInt(location), fullUrl);
            // TODO: Create a Breakthrough REST proxy that connects to a
            // game resource on a specific Location
        }

        // Start the interpreter
        ClientInterpreter interpreter =
                new ClientInterpreter(game,
                        System.in, System.out);
        interpreter.readEvalLoop();

    }
}
