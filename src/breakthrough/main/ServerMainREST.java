package breakthrough.main;

import breakthrough.domain.RESTServer;

/**
 * App server for Breakthrough, using REST.
 */
public class ServerMainREST {

    public ServerMainREST() throws Exception {
        int port = 4567;

        // TODO: Create the REST based server instance

        // Welcome
        System.out.println("=== Breakthrough REST (port:" + port + ") ===");
        System.out.println(" Use ctrl-c to terminate!");
        new RESTServer();


        // and start it by registrering the routes to listen to
        // ala 'serverRequestHandler.registerRoutes();

    }

    public static void main(String[] args) throws Exception {
        new ServerMainREST(); // No error handling!
    }
}
