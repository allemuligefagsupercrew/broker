package breakthrough.domain;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.tools.ant.taskdefs.condition.Http;
import spark.*;

import javax.servlet.http.HttpServletResponse;

import static spark.Spark.*;


public class RESTServer {

    private RESTAdapter adapter;
    private Gson gson = new Gson();

    public RESTServer() {
        Spark.stop();
        Spark.port(4567);

        adapter = new RESTAdapter();
        String baseUrl = "/Breakthrough/";

        //Create Game
        put(baseUrl + "new", (req, res) -> {
            int id = adapter.createGame();
            res.header("Location", baseUrl + Integer.toString(id));
            res.status(HttpServletResponse.SC_CREATED);
            return "";
        });

        //Get Game
        get(baseUrl + ":id/game", (req, res) -> {
            GameState game = null;
            try {
                int id = Integer.parseInt(req.params(":id"));
                game = adapter.getGame(id);

                if (game == null) {
                    res.status(HttpServletResponse.SC_NOT_FOUND);
                }
                else{
                    res.status(HttpServletResponse.SC_OK);
                    return game;
                }
            } catch (NumberFormatException e) {
                res.status(HttpServletResponse.SC_BAD_REQUEST);
            }
            return game;
        }, json());

        //Move
        post(baseUrl + ":id/move", (req, res) -> {
            try {
                int id = Integer.parseInt(req.params(":id"));
                Move move = gson.fromJson(req.body(), Move.class);

                Move result = adapter.move(id, move);
                if (result == null) {
                    res.status(HttpServletResponse.SC_NOT_FOUND);
                    return "";
                }
                if (MoveState.ACCEPTED.equals(result.getStatus())) {
                    res.status(HttpServletResponse.SC_OK);
                } else {
                    res.status(HttpServletResponse.SC_FORBIDDEN);
                }
                return result;
            } catch (NumberFormatException | JsonSyntaxException e) {
                res.status(HttpServletResponse.SC_BAD_REQUEST);
            }
            return "";
        }, json());

        after(new Filter() {
            @Override
            public void handle(Request request, Response response) throws Exception {
                //For debugging the URL's
                System.out.println(request.url());

            }
        });

        delete(baseUrl + ":id/delete", (req, res) -> {
            try {
                int id = Integer.parseInt(req.params(":id"));

                boolean result = adapter.delete(id);
                if (result) {
                    res.status(HttpServletResponse.SC_OK);
                    return null;
                } else {
                    res.status(HttpServletResponse.SC_NOT_FOUND);
                    return null;
                }
            } catch (NumberFormatException e) {
                res.status(HttpServletResponse.SC_BAD_REQUEST);
            }
            return null;
        });
    }

    private ResponseTransformer json() {
        return JsonUtil::toJson;
    }

    public static class JsonUtil {
        static String toJson(Object object) {
            return new Gson().toJson(object);
        }
    }
}
