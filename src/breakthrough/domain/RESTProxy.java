package breakthrough.domain;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;


public class RESTProxy implements Breakthrough {

    private HttpResponse<String> resp;
    private int id;
    private String url;
    private GameState cache;
    private Calendar cacheExpiration;
    private Gson gson = new Gson();

    public RESTProxy(String url) throws UnirestException {
        this.url = url;


        HttpResponse<String> gameResp = Unirest.put(url + "/Breakthrough/new").asString();

        if (gameResp.getStatus() == 201) {
            id = Integer.parseInt(gameResp.getHeaders().get("Location").get(0).split("/")[2]);

            refreshCachedGameState();
        }
        if (cache == null) {
            throw new RuntimeException("Failed creating game.");
        }
    }

    public RESTProxy(int id, String url) throws UnirestException {
        this.id = id;
        this.url = url;
        refreshCachedGameState();
        resp = Unirest.get(url + "/Breakthrough/" + id + "/game").asString();
    }

    /**
     * @return the {@link GameState} from the game
     */
    @Override
    public GameState getState() {
        return cache; //Cache?
    }

    /**
     * Return the type of piece on a given position on the chess board.
     *
     * @param p
     * @return the type of piece on the location.
     */
    @Override
    public Color getPieceAt(Position p) {
        if (cacheHasExpired()) {
            refreshCachedGameState();
        }
        return cache.get(p);

    }

    /**
     * Return the player that is in turn, i.e. allowed to move.
     *
     * @return the player that may move a piece next
     */
    @Override
    public Color getPlayerInTurn() {
        if (cacheHasExpired()) {
            refreshCachedGameState();
        }
        return cache.getPlayerInTurn();
    }

    private boolean cacheHasExpired() {
        return Calendar.getInstance().after(cacheExpiration);
    }

    /**
     * Return the winner of the game.
     *
     * @return the winner of the game or null in case no winner has been found
     * yet.
     */
    @Override
    public Color getWinner() {
        //No test of win conditions
        return Color.NONE;
    }

    /**
     * Validate and potentially make a move. A move is invalid if you try to move
     * your opponent's pieces or the move does not follow the rules, see the
     * exercise specification. PRECONDITION: the (row,column) coordinates are
     * valid positions, that is, all between (0..7).
     *
     * @param move
     * @return true if the move is valid, false otherwise
     */
    @Override
    public boolean move(Move move) {
        try {
            HttpResponse<String> resp = Unirest.post(url + "/Breakthrough/" + id + "/move").body(gson.toJson(move)).asString();
            if (resp.getStatus() == 404) {
                throw new RuntimeException("MoveObject not returned - 404");
            }
            Move returnedMove = gson.fromJson(resp.getBody(), Move.class);
            return MoveState.ACCEPTED.equals(returnedMove.getStatus());
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void refreshCachedGameState() {
        try {

            HttpResponse<String> stringHttpResponse = Unirest.get(url + "/Breakthrough/" + id + "/game").asString();

            if (stringHttpResponse.getStatus() == HttpServletResponse.SC_OK) {
                cache = gson.fromJson(stringHttpResponse.getBody(), GameState.class);
                Calendar instance = Calendar.getInstance();
                instance.add(Calendar.SECOND, 3);
                cacheExpiration = instance;
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
}
