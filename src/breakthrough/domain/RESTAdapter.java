package breakthrough.domain;

import java.util.HashMap;
import java.util.Map;

public class RESTAdapter {

    private Map<Integer, Breakthrough> games;

    public RESTAdapter() {
        games = new HashMap<>();
    }

    public int createGame() {
        int id = games.size() + 1;
        games.put(id, new BreakthroughSurrogate());
        return id;
    }

    public GameState getGame(int gameId) {
        Breakthrough game = games.get(gameId);
        if (game != null) {
            return game.getState();
        }
        return null;
    }

    public Move move(int gameId, Move move) {
        Breakthrough game = games.get(gameId);
        if (game == null) {
            return null;
        }
        boolean success = game.move(move);
        Move result = new Move(move.getFrom(), move.getTo());
        if (success) {
            result.changeState(MoveState.ACCEPTED);
        } else {
            result.changeState(MoveState.REJECTED);
        }
        return result;
    }

    public boolean delete(int id) {
        Breakthrough remove = games.remove(id);
        if (remove == null) {
            return false;
        }
        return true;
    }
}
