package breakthrough.domain;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import frs.broker.Constants;
import frs.broker.Invoker;
import frs.broker.ReplyObject;

import javax.servlet.http.HttpServletResponse;

public class BreakthroughInvoker implements Invoker {


    private final Breakthrough breakthrough;
    private final Gson gson;

    public BreakthroughInvoker(Breakthrough breakthrough) {
        this.breakthrough = breakthrough;
        gson = new Gson();
    }

    @Override
    public ReplyObject handleRequest(String objectId,
                                     String operationName,
                                     String payloadJSONArray) {
        ReplyObject reply = null;

    /*
     * To support multiple argument methods the parameters are
     * marshalled into a JSONArray of potentially mixed types.
     * This is a bit complex to demarshall, please review the
     * Gson docs + example (RawCollectionsExample) which is
     * the method used here.
     */

        // Demarshall parameters into a JsonArray
        JsonParser parser = new JsonParser();
        JsonArray array =
                parser.parse(payloadJSONArray).getAsJsonArray();

        try {
            // Dispatching on all known operations
            // Each dispatch follows the same algorithm
            // a) retrieve parameters from json array (if any)
            // b) invoke servant method
            // c) populate a reply object with return values

            if (operationName.equals(Constants.GET_PIECE_AT)) {
                // Parameter convention: [0] = Position
                Position position = gson.fromJson(array.get(0), Position.class);

                Color pieceAt = breakthrough.getPieceAt(position);
                int statusCode =
                        (pieceAt == Color.NONE) ?
                                HttpServletResponse.SC_NOT_FOUND :
                                HttpServletResponse.SC_OK;

                reply = new ReplyObject(statusCode, gson.toJson(pieceAt));

            } else if (operationName.equals(Constants.GET_PLAYER_IN_TURN)) {

                Color playerInTurn = breakthrough.getPlayerInTurn();

                reply = new ReplyObject(HttpServletResponse.SC_OK, gson.toJson(playerInTurn));

            } else if (operationName.equals(Constants.GET_WINNER)) {

                Color winner = breakthrough.getWinner();

                reply = new ReplyObject(HttpServletResponse.SC_OK, gson.toJson(winner));

            } else if (operationName.equals(Constants.MOVE)) {
                Move move = gson.fromJson(array.get(0), Move.class);

                boolean moveSuccess = breakthrough.move(move);

                reply = new ReplyObject(HttpServletResponse.SC_OK, gson.toJson(moveSuccess));

            } else {
                // Unknown operation
                reply = new ReplyObject(HttpServletResponse.
                        SC_NOT_IMPLEMENTED,
                        "Server received unknown operation name: '"
                                + operationName + "'.");
            }

        } catch (RuntimeException e) {
            reply = new ReplyObject(
                    HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    e.getMessage());
        }
        return reply;
    }

}
