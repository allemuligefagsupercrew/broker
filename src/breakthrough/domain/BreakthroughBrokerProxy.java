package breakthrough.domain;

import frs.broker.ClientProxy;
import frs.broker.Constants;
import frs.broker.Requestor;

public class BreakthroughBrokerProxy implements Breakthrough, ClientProxy {
    private final Requestor req;

    public BreakthroughBrokerProxy(Requestor req) {
        this.req = req;
    }

    /**
     * @return the {@link GameState} from the game
     */
    @Override
    public GameState getState() {
        return null;
    }

    /**
     * Return the type of piece on a given position on the chess board.
     *
     * @param p
     * @return the type of piece on the location.
     */
    @Override
    public Color getPieceAt(Position p) {
        return req.sendRequestAndAwaitReply(Constants.BT_ID, Constants.GET_PIECE_AT, Color.class, p);
    }

    /**
     * Return the player that is in turn, i.e. allowed to move.
     *
     * @return the player that may move a piece next
     */
    @Override
    public Color getPlayerInTurn() {
        return req.sendRequestAndAwaitReply(Constants.BT_ID, Constants.GET_PLAYER_IN_TURN, Color.class);
    }

    /**
     * Return the winner of the game.
     *
     * @return the winner of the game or null in case no winner has been found
     * yet.
     */
    @Override
    public Color getWinner() {
        return req.sendRequestAndAwaitReply(Constants.BT_ID, Constants.GET_WINNER, Color.class);
    }

    /**
     * Validate and potentially make a move. A move is invalid if you try to move
     * your opponent's pieces or the move does not follow the rules, see the
     * exercise specification. PRECONDITION: the (row,column) coordinates are
     * valid positions, that is, all between (0..7).
     *
     * @param move
     * @return true if the move is valid, false otherwise
     */
    @Override
    public boolean move(Move move) {
        return req.sendRequestAndAwaitReply(Constants.BT_ID, Constants.MOVE, Boolean.TYPE, move);
    }
}
