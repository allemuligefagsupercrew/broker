package frs.broker;

/**
 * Define the current marshalling format version.
 *
 * Created by Henrik Baerbak Christensen, AU.
 */
public class Constants {
    public static final int MARSHALLING_VERSION = 1;
    public static final String GET_PIECE_AT = "getPieceAt";
    public static final String GET_PLAYER_IN_TURN = "getPlayerInTurn";
    public static final String GET_WINNER = "getWinner";
    public static final String MOVE = "move";

    public static final String BT_ID = String.valueOf(42);
}
