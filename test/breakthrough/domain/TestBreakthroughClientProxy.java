package breakthrough.domain;

import frs.broker.Constants;
import frs.broker.Requestor;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestBreakthroughClientProxy {
    private Breakthrough game;
    private TestSpyRequestor spy;
    private Color col;

    @Before
    public void setUp() {
        // Create a test spy for the requestor,
        // and inject it into the client proxy
        spy = new TestSpyRequestor();
        game = new BreakthroughBrokerProxy(spy);
    }

    @Test
    public void shouldValidateClientProxyCallingRequestorForGetWinner() {
        // Invoke a method for testing
        col = game.getWinner();

        // and validate that the client proxy produce proper indirect output from invoked method
        assertThat(spy.lastOID, is("42"));

        assertThat(spy.lastArgument.length, is(0));
        assertTrue(spy.lastType.equals(Color.class));
        assertThat(spy.lastOperationName, is(Constants.GET_WINNER));
    }

    @Test
    public void shouldValidateClientProxyCallingRequestorForGetPieceAt() {
        // ...etc. for all methods
        Position p = new Position(0, 0);
        col = game.getPieceAt(p);

        assertThat(spy.lastOID, is("42"));
        assertThat(spy.lastArgument[0], is(p));
        assertTrue(spy.lastType.equals(Color.class));
        assertThat(spy.lastOperationName, is(Constants.GET_PIECE_AT));

    }

    @Test
    public void shouldValidateClientProxyCallingRequestorForGetPlayerInTurn() {
        col = game.getPlayerInTurn();

        assertThat(spy.lastOID, is("42"));
        assertThat(spy.lastArgument.length, is(0));
        assertTrue(spy.lastType.equals(Color.class));
        assertThat(spy.lastOperationName, is(Constants.GET_PLAYER_IN_TURN));
    }

    @Test
    public void shouldValidateClientProxyCallingRequestorForMove() {
        Move m = new Move(new Position(6, 0), new Position(5, 0));
        game.move(m);

        assertThat(spy.lastOID, is("42"));
        assertThat(spy.lastArgument[0], is(m));
        assertTrue(spy.lastType.equals(Boolean.TYPE));
        assertThat(spy.lastOperationName, is(Constants.MOVE));
    }

    private class TestSpyRequestor implements Requestor {
        private String lastOID;
        private String lastOperationName;
        private Object[] lastArgument;
        private Type lastType;

        /**
         * Marshall the given operation and its parameters into a request object, send
         * it to the remote component, and interpret the answer and convert it back
         * into the return type of generic type T
         *
         * @param objectId          the object that this request relates to; not that this may not
         *                          necessarily just be the object that the method is called upon
         * @param operationName     the operation (=method) to invoke
         * @param typeOfReturnValue the java reflection type of the returned type
         * @param argument          the arguments to the method call
         * @return the return value of the type given by typeOfReturnValue
         */
        @Override
        public <T> T sendRequestAndAwaitReply(String objectId, String operationName, Type typeOfReturnValue, Object... argument) {
            lastOID = objectId;
            lastOperationName = operationName;
            lastArgument = argument;
            lastType = typeOfReturnValue;
            return typeOfReturnValue.equals(Boolean.TYPE) ? (T) Boolean.FALSE : null;
        }
    }
}
