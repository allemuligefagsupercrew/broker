package breakthrough.domain;

import frs.broker.*;
import frs.broker.marshall.json.StandardJSONRequestor;
import org.junit.Before;
import org.junit.Test;
import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.*;


public class TestBreakthroughInvoker {
    private LocalClientRequestHandler clientRequestHandler;
    private BreakthroughBrokerProxy proxy;


    @Before
    public void setUp(){
        BreakthroughInvoker invoker = new BreakthroughInvoker(new BreakthroughSurrogate());
        clientRequestHandler = new LocalClientRequestHandler(invoker);
        StandardJSONRequestor requestor = new StandardJSONRequestor(clientRequestHandler);
        proxy = new BreakthroughBrokerProxy(requestor);
    }


    @Test
    public void shouldVerifyMarshallingFormatForGetPieceAt00(){
        proxy.getPieceAt(new Position(0,0));

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(),is(Constants.GET_PIECE_AT));
        assertThat(req.getPayload(),containsString("\"row\":0,\"column\":0"));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getVersionIdentity(), is(Constants.MARSHALLING_VERSION));
        assertThat(rep.getPayload(),containsString("BLACK"));
    }

    @Test
    public void shouldGiveStatusCodeNotFoundForGetPieceAt33(){
        proxy.getPieceAt(new Position(4,4));

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(),is(Constants.GET_PIECE_AT));
        assertThat(req.getPayload(),containsString("\"row\":4,\"column\":4"));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_NOT_FOUND));
        assertThat(rep.getVersionIdentity(), is(Constants.MARSHALLING_VERSION));
        assertThat(rep.getPayload(),containsString("NONE"));
    }

    @Test
    public void shoulderVerifyMarshallingFormatForGetPlayerInTurn(){
        proxy.getPlayerInTurn();

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(),is(Constants.GET_PLAYER_IN_TURN));
        assertThat(req.getPayload(), is("[]"));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getVersionIdentity(), is(Constants.MARSHALLING_VERSION));
        assertThat(rep.getPayload(),containsString("WHITE"));
    }

    @Test
    public void shouldVerifyMarshallingFormatForGetWinner(){
        proxy.getWinner();

        RequestObject req = clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(),is(Constants.GET_WINNER));
        assertThat(req.getPayload(), is("[]"));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getVersionIdentity(), is(Constants.MARSHALLING_VERSION));
        assertThat(rep.getPayload(),containsString("NONE"));
    }

    @Test
    public void shouldVerifyMarshallingFormatForMove(){
        proxy.move(new Move(new Position(6,6), new Position(5,5)));

        RequestObject req= clientRequestHandler.getLastRequest();
        assertThat(req.getOperationName(),is(Constants.MOVE));
        assertThat(req.getPayload(), containsString("\"row\":6,\"column\":6"));
        assertThat(req.getPayload(), containsString("\"row\":5,\"column\":5"));

        ReplyObject rep = clientRequestHandler.getLastReply();
        assertThat(rep.getStatusCode(), is(HttpServletResponse.SC_OK));
        assertThat(rep.getVersionIdentity(), is(Constants.MARSHALLING_VERSION));
        assertThat(rep.getPayload(),containsString("true"));
    }

    public class LocalClientRequestHandler implements ClientRequestHandler{
        private final BreakthroughInvoker invoker;
        private ReplyObject lastReply;
        private RequestObject lastRequest;

        public LocalClientRequestHandler(BreakthroughInvoker invoker){
            this.invoker = invoker;
        }

        /**
         * Send a request, defined by an operation name and a payload (in the chosen
         * marshaling format), to the server's server request handler; await an answer
         * and return a valid reply object. The objectId can be interpreted in a broad
         * sense (not necessarily as the id of 'obj' in 'obj.operation(params)'),
         * depending upon the invoker at the server side.
         *
         * @param requestObject the request to send
         * @return a reply from the remote component
         * @throws RuntimeException in case some error happened in the IPC
         */
        @Override
        public ReplyObject sendToServer(RequestObject requestObject) {
            lastRequest = requestObject;

            lastReply = invoker.handleRequest(requestObject.getObjectId(),
                    requestObject.getOperationName(),
                    requestObject.getPayload());
            return lastReply;
        }

        public ReplyObject getLastReply() {
            return lastReply;
        }

        public RequestObject getLastRequest() {
            return lastRequest;
        }
    }

}

