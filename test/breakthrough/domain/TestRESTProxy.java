package breakthrough.domain;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TestRESTProxy {

    private RESTProxy proxy;

    @Before
    public void setUp() throws UnirestException {
        RESTAdapter adapter = new RESTAdapter();
        int id = adapter.createGame();
        proxy = new RESTProxy( "http://localhost:4567");
    }

    @Test
    public void test(){
        assertThat(proxy.getPlayerInTurn(),is(Color.WHITE));
    }
}
