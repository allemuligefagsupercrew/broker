package breakthrough.domain;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class TestRESTAdapter {
    private RESTAdapter adapter;
    private int id;

    @Before
    public void setUp() {
        adapter = new RESTAdapter();
        id = adapter.createGame();
    }

    @Test
    public void shouldHaveGameCreatedAndRetrieveIt() {
        assertNotNull(adapter.getGame(id));
    }

    @Test
    public void shouldRejectMoveNoPiece() {
        assertThat(adapter.move(id, new Move(new Position(4, 0), new Position(4, 1))).getStatus(), is(MoveState.REJECTED));
    }

    @Test
    public void shouldAcceptMoveWhitePlayerInTurn(){
        assertThat(adapter.move(id, new Move(new Position(6, 0), new Position(5, 0))).getStatus(), is(MoveState.ACCEPTED));
    }

    @Test
    public void shouldRejectMoveWhitePlayerInTurn(){
        assertThat(adapter.move(id, new Move(new Position(1, 0), new Position(3, 0))).getStatus(), is(MoveState.REJECTED));
    }

    @Test
    public void shouldDeleteGame() {
        assertThat(adapter.delete(id), is(true));
        assertNull(adapter.getGame(id));
        assertThat(adapter.delete(id), is(false));
    }


}
